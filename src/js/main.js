console.log("%cHi there! ^_^", "font-family: Arial, sans-serif; font-weight: bold; font-size: 20px");
console.log("%cWanna know how this was made? Click on the source line over there to know more.", "font-family: Arial, sans-serif; font-size: 14px");
// Hey there, welcome, let's begin!

// Here, I've created a custom function to emulate a $document.ready() callback
// It ensures logic executes only when everything is properly loaded
const ready = (fn) => {
  if (document.readyState != "loading") {
    fn();
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
};

// The release date of FF7 Rebirth, being this worldwide, it makes sense it
// will be released at the start of that day. The date is immediately parsed.
const targetTime = new Date("2024-02-29T00:00:00").getTime();

// This function calculates how many "discs" are left, a "disc" being a unit of 50 hours
// If you're reading this and have no idea what I mean, check this clip https://clips.twitch.tv/ImportantCoyPeppermintKippa-UoYudqLJI0ceb10w
const getDiscsLeft = () => {
  // The current time is obtained
  const currentTime = new Date().getTime();

  // And finally calculate the time difference in milliseconds
  const timeDifference = targetTime - currentTime;

  // If the target date has already passed
  if (timeDifference < 0) {
    // It ends the function by returning a zero, signaling the date has already passed
    return 0;
  } else {
    // Calculate the number of discs left (again, each disc = 50 hours)
    const discsLeft = (timeDifference / (1000 * 60 * 60 * 50)).toFixed(7);
    return discsLeft;
  }
  // This function is set to being executed every time the screen is refreshed
}, updateCountdown = () => {
  const discsLeft = getDiscsLeft();

  if (discsLeft > 0.0) {
    // Instead of do math here, I've decided to manipulate the number as a string
    // First, the elements that will contain the units and decimals of the number are obtained
    // The number is then separated into its integer and decimal parts.
    const unitsElement = document.getElementById("disc-units"),
      decimalsElement = document.getElementById("disc-decimals"),
      discsLeftString = discsLeft.toString().split(".");

    unitsElement.textContent = discsLeftString[0];
    decimalsElement.textContent = `.${discsLeftString[1]}`;
    // requestAnimationFrame will execute the function in its argument the next time the screen is repainted. This usually occurs when your screen refreshes.
    requestAnimationFrame(updateCountdown);
  } else {
    document.getElementById("disc-countdown").remove();
    document.getElementById("discs").classList.add("mt-5");
    document.getElementById("discs").textContent = "No discs left. Go enjoy the game!";
  }
  // This will execute every time the screen is resized, as a utility to prevent styling artifacts on some screens.
}, onWindowResize = () => {
  document.documentElement.classList.toggle("h-100", document.documentElement.scrollHeight <= window.innerHeight);
  document.body.classList.toggle("h-100", document.documentElement.scrollHeight <= window.innerHeight);
};

// This will be executed as soon as the document is loaded enough to use this logic with no issues
ready(() => {
  // This helper indicates how long it took to reach this point, starting with
  // console.time("DOMReady"), defined at the very beginning of the document
  console.timeEnd("DOMReady");
  // The following takes care of the loading spinner
  document.getElementById("loading").classList.add("loading-done");
  const currentYear = new Date().getFullYear();
  // This will initialize all bootstrap popovers
  [...document.querySelectorAll('[data-bs-toggle="popover"]')].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl, {
    trigger: 'focus'
  }));
  // This will initialize all bootstrap tooltips
  [...document.querySelectorAll('[data-bs-toggle="tooltip"]')].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));
  // This will initialize the rest
  updateCountdown();
  onWindowResize();
  window.onresize = onWindowResize;
  // This will modify the copyright year, so it updates correctly from 2024 forwards
  if (currentYear > 2023) {
    document.getElementById("year").textContent = ` - ${currentYear}`;
  }
  // I hope this is enough for you to understand. If not, you can DM me at https://twitter.com/BreadMakerCTM or send me a message at https://t.me/breadmaker. Cheers!
});